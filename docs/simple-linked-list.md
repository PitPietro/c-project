# Simple Linked List

## 1. Introduction

### 1.1. Concept of List
A simple list is a sequnce of elements of the same data type
 Just like all ADTs, the list is defined in terms of:
1. **domain** of the elements of the list
2. **construction** and **selection** operations
3. **predicates**

### 1.2. Sequential & Linked List
The **sequential list** is represented with an array: the list elements are allocated in adjacent locations.

The **linked list**'s elements are associated with the address of the successive element's position (a pointer).

The list element is called **node**, the index is called **arc**.

### Implementation with Pointers
In the list implementation with pointers, each list node is a structure made up by two fields:
1. element's value
2. successive node's position: a pointer (`NULL` if is the last element)

```c
// linked-list/globals.h

typedef struct linked_list {
    int value;
    struct linked_list *next;
} item;

typedef item *list;

list root = NULL;
```

## 2. Operations over a List
Take a look at the [`linked-list`](../data-types/adt/linked-list/) folder.

The files common to all the operations are: 
- [`globals.h`](../data-types/adt/linked-list/simple-linked-list/globals.h)
- [`main.c`](../data-types/adt/linked-list/simple-linked-list/main.c)

### 2.1. Add an element on the head or in the tail of a simple list
- [`insert.h`](../data-types/adt/linked-list/simple-linked-list/insert.h)
- [`insert.c`](../data-types/adt/linked-list/simple-linked-list/insert.c)

### 2.2. Search an element
Since there's no index to increment (you're not dealing with an array), you need a pointer with which you can sequentially (or recursively) scan the list:
- [`search.h`](../data-types/adt/linked-list/simple-linked-list/search.h)
- [`search.c`](../data-types/adt/linked-list/simple-linked-list/search.c)

### 2.3. Length of a list
- [`length.h`](../data-types/adt/linked-list/simple-linked-list/length.h)
- [`length.c`](../data-types/adt/linked-list/simple-linked-list/length.c)

## 3. Ordered List
An order relationship must be defined on the base-domain of the elements of the list.

The ordered insertion in the list can be:
- Iterative:
  scans the list with two auxiliary pointers, up to point where the element is to be inserted, and adds a node arranging the pointers
- Recurisive:
  1. without duplication
  2. duplicating a part of the list

The files are the same for both iterative and recursive ways:
- [`ordered-insert.h`](../data-types/adt/linked-list/simple-linked-list/ordered-insert.h)
- [`ordered-insert.c`](../data-types/adt/linked-list/simple-linked-list/ordered-insert.c)

### 3.1. Iterative insertion
How the algorithm works:
- scan the list until it reaches a node storing an element which is major than the one to insert
- allocate a new node with the element to insert
- connect the new node to the two adjacent ones

### 3.2. Recursive insertion
To insert an element, in an ordered way, in an ordered list:
- if the list is empty
  build a new list containing the new item
- otherwise, if the element to be inserted is less than the head of the list
  add the new item at the top of the given list,
- otherwise
  the element will be inserted later in the given list

The first two cases are elementary operations while the third case can be treated iteratively or going back to the same problem recursively, but on a simpler case:  it will be possible make either an insertion in the head or go back to the empty list

## 4. List of Struct

Pros:
1. the list must not be statically defined, it's a dynamic data structure, it grows as long as there space in the heap
2. change the type of the elements (value field), but the operations remain almost the same

Cons:
1. sequential search only
