#include <stdio.h>
#include <stdlib.h>

int f1();
int f2(int);

int main() {
    int i = (5, 10); // 10 is assigned to i

    int j = (f1(), f2(i)); // f1() is called (evaluated) first followed by f2().
    // the returned value of f2() is assigned to j

    printf("i = %d\n", i);
    printf("j = %d\n", j);

    return 0;
}

int f1() {
    printf("I'm inside f1()\n");
    return 1;
}

int f2(int i) {
    return i * 5;
}

// cd expressions/comma-operator/
// gcc -o comma comma.c && ./comma 