#include <stdio.h>

extern int global_a;

int main() {
    printf("a = %d\n", global_a);
}

// cd lifetime-scope/scope/external-linking
// gcc -o main main.c fun.c && ./main