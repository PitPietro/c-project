#include <stdio.h>
#include "add-factorials.h"

/**
 * @brief Adds the factorial of the given numbers
 * 
 * @param a 1st number
 * @param b 2nd number
 * @return int a! + b!
 */
long addition_between_factorials(int a, int b) {
    printf("addition between %d! and %d!\n", a, b);
    return private_factorial(a) + private_factorial(b);
}

/**
 * @brief Evaluate factorial of the given number
 * This function is private, so it's not accessible from other files.
 * 
 * @param n number to evaluate the factorial from
 * @return long n!
 */
static long private_factorial(int n) {
    int i = 1;
    long result = 1;

    while(i <= n) {
        // result = result * i
        result *= i;
        i++;
    }

    return result;
}