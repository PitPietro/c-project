#include <stdio.h>
#include <stdlib.h>
#include "add-factorials.h"

int main() {
    long result = addition_between_factorials(2, 5);
    printf("main.c --> add factorials: %ld\n", result);

    // printf("main.c --> try to access a file scope function: %ld\n", private_factorial(4));
    
    // the line above would give the error:
    // add-factorials.h:2:13: warning: ‘private_factorial’ used but never defined
    //     2 | static long private_factorial(int);
    //       |             ^~~~~~~~~~~~~~~~~
    // /usr/bin/ld: /tmp/ccDSRqJ0.o: in function `main':
    // main.c:(.text+0x3d): undefined reference to `private_factorial'
    // collect2: error: ld returned 1 exit status

    return 0;
}

// cd lifetime-scope/scope/internal-linking-for-functions
// gcc -o main main.c add-factorials.c && ./main