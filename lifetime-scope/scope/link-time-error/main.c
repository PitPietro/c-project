#include <stdio.h>

extern int b;

int main() {
    printf("b = %d\n", b);
}

// cd lifetime-scope/scope/link-time-error
// gcc -o main main.c hello.c && ./main